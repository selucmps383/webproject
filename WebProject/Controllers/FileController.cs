﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebProject.Controllers
{
    public class FileController : Controller
    {
        public IActionResult Create()
        {
            return Content("temporary workaround", "text/plain");
        }
    }
}